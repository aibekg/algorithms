const arr = [2, 23, 4, 54, -12, 1, -1];

const bubbleSort = (array) => {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length; j++) {
            if(array[j + 1] < array[j]) {
                const tmp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = tmp;
            }
        }
    }
    return array
}

console.log(bubbleSort(arr));