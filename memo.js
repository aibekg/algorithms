function memo() {
    const cache = new Map();

    function hardOperation (number) {
        if(cache.has(number)) {
            console.log(`${number} is cached`);
            return cache.get(number)
        } 

        let result = 1;

        for (let i = 0; i < number; i++) {
            result = result * number
        }

        cache.set(number, result)

        return result    
    }

    function getCache() {
        return cache
    }
    
    return [getCache, hardOperation]
}

const [getCache, memoFunc] = memo();

const data1 = memoFunc(5);

const data2 = memoFunc(10);

const data3 = memoFunc(5);


console.log(getCache());
console.log({data1, data2, data3});

