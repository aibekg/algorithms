const arr = [2, 43, 12, 3, 5, 32, 15];

const selectionSort = (array) => {
    for (let i = 0; i < array.length; i++) {
        let indexMin = i;

        for (let j = i + 1; j < array.length; j++) {
            if(array[j] < array[indexMin]) {
                indexMin = j
            }
        }
        
        const tmp = array[i];
        array[i] = array[indexMin];
        array[indexMin] = tmp;
        
    }
    
    return array;
}

console.log(selectionSort(arr))