// Input: [1, 2, 3, 4, 5, 6, 7, 8]
// Output: 
//      [1, 3, 4], [2, 1, 5], [5, 3], [6, 2], [7, 1]

/**
 * @param {Array<Array<number>>} value 
 * @param {Array<number>} target
 */
const checkExistance = (value, target) => {
    for (const item of value) {
        if(JSON.stringify(item.sort()) === JSON.stringify(target.sort())) {
            return true
        }
    }

    return false;
};

/**
 * @param {Array<number>} value 
 */
const getPosibleCompinations = (value) => {
    const result = [];
    const lastIndex = value.length - 1;
    const targetMaxValue = value[lastIndex];

    for (let i = 0; i < value.length; ++i) {
        const requiredValue = targetMaxValue - value[i];
        
        if (!value.includes(requiredValue)) continue;

        if (requiredValue === value[i]) {
            const peace = getPosibleCompinations(value.slice(0, i + 1));
            if (!peace.length) {
                continue;
            }

            result.push(peace.flat().push(value[i]));
            continue;
        }

        let peace = [value[i], requiredValue];

        if (!checkExistance(result, peace)) {
            result.push(peace);
            continue;
        }

        peace = getPosibleCompinations(value.filter(item => item <= requiredValue));

        if (!peace.length) {
            continue;
        }

        const targetPeace = peace[0];
        
        targetPeace.push(value[i]);
        result.push(targetPeace);
    }

    return result.reverse();
};

console.log(getPosibleCompinations([1, 2, 3, 4, 5, 6, 7, 8]));
