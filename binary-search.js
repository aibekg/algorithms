const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const binarySeach = (array, searchedItem) => {
    let
        start = 0, 
        middle,
        end = array.length;

    while (start <= end) {
        middle = Math.floor((start + end) / 2)

        if(array[middle] === searchedItem) {
            return true
        }

        if(array[middle] > searchedItem) {
            end = middle - 1
        } else {
            start = middle + 1
        }
    }

    return false
}

 console.log(binarySeach(arr, -20))

